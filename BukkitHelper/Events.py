import org.bukkit as Bukkit
import org.bukkit.plugin as Plugin
import org.bukkit.event as Event

class Executor(Plugin.EventExecutor):
    def __init__(self, callback, handler, name=None):
        self.Callback = callback
        self.Name = name
        self.Handler = handler
        
    
    def execute(self, listener, event):
        self.Callback(self, listener, event)

def EventListener(func, eventHandler, plugin, name=None):
    #eventHandler = Event.player.PlayerInteractEvent
    executor = Executor(func, eventHandler, name)
    handlerList = eventHandler.getHandlerList()
    
    regListener = Plugin.RegisteredListener(None, executor, Event.EventPriority.NORMAL, plugin, True)
    handlerList.register(regListener)
    
    