# -*- coding: utf-8 -*-

if pyrun_args != None:
    entitys = pyrun_args
else:
    entitys = me.buk

if not isinstance(entity, (list, tuple)):
    entitys = [entitys,]

from org.bukkit.potion import PotionEffectType
import re

potions = []

for prop in dir(PotionEffectType):
    if re.match("^[A-Z_]*$", prop):
        potion = getattr(PotionEffectType, prop)
        for ent in entitys:
            ent.removePotionEffect(potion)
