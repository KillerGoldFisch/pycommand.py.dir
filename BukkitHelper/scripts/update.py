#!/usr/bin/env python

__url__ = "https://gliewe.dyndns.org/owncloud/public.php?service=files&t=cb1c10a6e2e693c3a0b4d5b0fe2fb7d1&download&path=//"

__filelist__ = "__files__.txt"

import urllib2, os, traceback

directory = os.path.abspath(os.path.dirname(__file__))

fileList = urllib2.urlopen(__url__ + __filelist__).read()

fileList = fileList.split("\n")

for f in fileList:
    f += ".py"
    try:
        print "Try to download '%s' : "%f,
        fileContent = urllib2.urlopen(__url__ + f).read()
        if len(fileContent) < 2 or chr(0) in fileContent:
            print "FAIL"
            continue
        print "OK"
        print "Save File : ",
        stream = open(os.path.join(directory, f), "w")
        stream.write(fileContent)
        stream.close()
        print "OK"
    except Exception, e:
        print "FAIL: " + str(e)
        traceback.print_exc(file=sys.stdout)

