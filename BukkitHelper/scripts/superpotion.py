# -*- coding: utf-8 -*-

if pyrun_args != None:
    entitys = pyrun_args
else:
    entitys = me.buk

if not isinstance(entity, (list, tuple)):
    entitys = [entitys,]


from org.bukkit.potion import PotionEffectType

effects = [
    PotionEffectType.DAMAGE_RESISTANCE,
    PotionEffectType.FAST_DIGGING,
    PotionEffectType.FIRE_RESISTANCE ,
    PotionEffectType.HEAL ,
    PotionEffectType.INCREASE_DAMAGE ,
    PotionEffectType.INVISIBILITY ,
    PotionEffectType.JUMP ,
    PotionEffectType.NIGHT_VISION ,
    PotionEffectType.REGENERATION ,
    PotionEffectType.SPEED ,
    ]

for effectType in effects:
    effect = effectType.createEffect(99999, 20)
    for ent in entitys:
        ent.addPotionEffect(effect, True)