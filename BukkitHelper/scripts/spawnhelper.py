# -*- coding: utf-8 -*-

if pyrun_args is not None:
    countOfHelpers = pyrun_args
else:
    countOfHelpers = 1

import org.bukkit.entity

loc = me.buk.getLocation()
world = loc.getWorld()

for n in xrange(countOfHelpers):
    wolf = world.spawn(loc, org.bukkit.entity.Wolf)
    wolf.setTamed(True)
    wolf.setOwner(me.buk)