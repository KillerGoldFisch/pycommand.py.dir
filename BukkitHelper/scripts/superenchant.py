# -*- coding: utf-8 -*-
from org.bukkit.enchantments import Enchantment

itemStack = me.buk.getItemInHand()

enchantments = [
    Enchantment.ARROW_DAMAGE,
    Enchantment.ARROW_FIRE,
    Enchantment.ARROW_INFINITE,
    Enchantment.ARROW_KNOCKBACK,
    Enchantment.DAMAGE_ALL,
    Enchantment.DAMAGE_ARTHROPODS,
    Enchantment.DAMAGE_UNDEAD,
    Enchantment.DIG_SPEED,
    Enchantment.DURABILITY,
    Enchantment.FIRE_ASPECT,
    Enchantment.KNOCKBACK,
    Enchantment.LOOT_BONUS_BLOCKS,
    Enchantment.LOOT_BONUS_MOBS,
    Enchantment.OXYGEN,
    Enchantment.PROTECTION_ENVIRONMENTAL,
    Enchantment.PROTECTION_EXPLOSIONS,
    Enchantment.PROTECTION_FALL,
    Enchantment.PROTECTION_FIRE,
    Enchantment.PROTECTION_PROJECTILE,
    Enchantment.SILK_TOUCH,
    Enchantment.THORNS,
    Enchantment.WATER_WORKER,
    ]

for ench in enchantments:
    for n in range(10, 0, -1):
        try:
            itemStack.addEnchantment(ench, n)
            break
        except:
            pass