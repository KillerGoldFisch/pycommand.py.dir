import Events, Console

#from org.bukkit.entity import Player as bPlayer

isOnline = True

def OnShutdown():
    pass

def getPlayer(bplayer):
    if isinstance(bplayer, str):
        bplayer = Server.getPlayer(bplayer)
    if Player._players.has_key(bplayer):
        return Player._players[bplayer]
    else:
        return Player(bplayer)

############################################################################
############################## Player ######################################
############################################################################

class Player:

    _players = {}

    @staticmethod
    def _staticcron():
        for key, player in Player._players.items():
            try:
                player._cron()
            except:
                pass

    def _cron(self):
        if not self.buk.isOnline():
            Player._players.pop(self.buk, None)
        for job in self.CronJobs.itervalues():
            try:
                job(self)
            except:
                pass

    def __init__(self, bplayer):
        import goodshelve, os
        self.buk = bplayer
        self.CronJobs = {}
        self._console = None
        self.DB = goodshelve.open(os.path.join(BukkitHelperDBDir, bplayer.getName() + ".bin"))
        self.DB["me"] = self
        Player._players[bplayer] = self

    def get(self, key, default = None):
        if vars(self).has_key(key):
            return vars(self)[key]
        return self.DB[key]

    def set(self, key, value):
        self.DB[key] = value

    def shootArrow(self):
        import org.bukkit.entity as Entity
        arrow = self.buk.launchProjectile(Entity.Arrow)
        arrow.setFireTicks(42)
        arrow.setVelocity(arrow.getVelocity().multiply(3.0))
        arrow.setTicksLived(5900)
        return arrow

    def shootSkull(self):
        import org.bukkit.entity as Entity
        skull = self.buk.launchProjectile(Entity.WitherSkull)
        skull.setVelocity(skull.getVelocity().multiply(3.0))
        return skull

    def getConsole(self):
        if self._console == None:
            self._console = Console.customConsoleClass(self.DB)
            self._console.push("from org.bukkit import Material")
            self._console.push("from BukkitHelper.Utils import GThread")
            self._console.push("form Console.ConsoleHelper import *")
            import BlockProxy
            if not self.DB.has_key("targets"):
                self.DB["targets"] = BlockProxy.BlockList()
            if not self.DB.dict.has_key("aimed"):
                self.DB.dict["aimed"] = list()
                self.DB.dict["lastaimed"] = None
        return self._console

    def getName(self):
        return self.buk.getPlayerListName()

    def resetConsole(self):
        self._console = None

    def setPermission(self, key, Value):
        import java.lang
        import Share
        if Value:
            self.buk.addAttachment(Share.Plugin, key, java.lang.Boolean.TRUE).setPermission(key, java.lang.Boolean.TRUE)
        else:
            self.buk.addAttachment(Share.Plugin, key, java.lang.Boolean.FALSE).setPermission(key, java.lang.Boolean.FALSE)

    def __repr__(self):
        return "<Player '%s'>"%self.getName()


    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        return getattr(self.buk, name)

    def getAimedEntity(self):
        position1 = self.buk.getLocation().toVector() #Vector
        facing = self.buk.getLocation().getDirection() #Vector

        world = self.buk.getLocation().getWorld()

        actualEntity = None
        actualDist = 9999

        #print "position1=%s facing=%s"%(str(position1), str(facing))

        for entity in world.getLivingEntities(): #self.buk.getNearbyEntities(100,100,100): #

            entityPosition = entity.getLocation().toVector() #Vector

            aimdist = facing.clone().crossProduct(entityPosition.clone().subtract(position1)) \
                .length()/facing.length()



            #print "%s -> %f"%(str(entity), aimdist)
            if aimdist < 1.5 and entity != self.buk:
                distToPlayer = position1.clone().distance(entityPosition) #double
                if distToPlayer < actualDist:
                    actualDist = distToPlayer
                    actualEntity = entity

        return actualEntity

"""
        position1 = self.buk.getLocation().toVector() #Vector
        facing = self.buk.getLocation().getDirection() #Vector
        position2 = facing.clone().add(facing.multiply(100.0)) #Vector

        world = self.buk.getLocation().getWorld()

        actualEntity = None
        actualDist = 9999

        for entity in world.getLivingEntities(): #self.buk.getNearbyEntities(100,100,100): #

            entityPosition = entity.getLocation().toVector() #Vector
            entityRelativePosition1 = entityPosition.clone().subtract(position1) #Vector
            entityRelativePosition2 = entityPosition.clone().subtract(position2) #Vector
            crossProduct = entityRelativePosition1.clone().crossProduct(entityRelativePosition2) #Vector
            distanceSquared = crossProduct.lengthSquared()/10000.0 #double
            print "%s -> %s"%(str(entity), distanceSquared)
            if distanceSquared < 0.3 and entity != self.buk:
                distToPlayer = position1.distance(entityPosition) #double
                print "%s -> %s"%(str(entity), distanceSquared)
                if distToPlayer < actualDist:
                    actualDist = distToPlayer
                    actualEntity = entity
"""




###############################################################################
########################### PlayerList ########################################
###############################################################################

def getPlayerList(stringlist):
    stringlist = stringlist.strip()
    stringlist = stringlist.split(" ")
    pList = []
    for playerName in stringlist:
        bplayer = Server.getPlayer(playerName)
        #print "search for '%s' -> '%s'"%(playerName, bplayer)
        if bplayer is not None:
            pList.append(Player(bplayer))

    return PlayerList(pList)

class PlayerList:
    def __init__(self, pList):
        self._playerList = pList

    def getPlayers(self):
        return self._playerList

    def __repr__(self):
        return str(self._playerList)

    def __getattr__(self, name):
        if name in self.__dict__:
            return self.__dict__[name]
        def method(*args, **kargs):
            ret = ""
            for player in self._playerList:
                m = getattr(player, name)
                ret += str(m(*args, **kargs))
            return ret
        return method




###############################################################################
########################### Cronloop ##########################################
###############################################################################

from thread import start_new_thread
def _cronloop(dummy):
    import time
    cron_time = 0.5
    last_time = time.time()
    while isOnline:
        timeToSleep = last_time - time.time() + cron_time
        timeToSleep = timeToSleep if timeToSleep > 0.0 else 0.0
        time.sleep(timeToSleep)
        last_time = time.time()
        #print "%s - %s"%(time.time(), timeToSleep)
        try:
            Player._staticcron()
        except:
            pass
#start_new_thread(_cronloop, (None,))