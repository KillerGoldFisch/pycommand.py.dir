DEBUG=False

__version__ = "0.1"

import os
BukkitHelperDir = os.path.abspath(os.path.dirname(__file__))
BukkitHelperDBDir = os.path.join(BukkitHelperDir, "DB")
isShutdown = False

if DEBUG:
    import sys; sys.path.append(r'/home/kevin/.eclipse/org.eclipse.platform_3.8_155965261/plugins/org.python.pydev_2.7.1.2012100913/pysrc/')
    import pydevd
    pydevd.settrace()

import org.bukkit as Bukkit
Server = Bukkit.Bukkit.getServer()

import Share

import BlockProxy
BlockProxy.BukkitHelperDir = BukkitHelperDir
BlockProxy.BukkitHelperDBDir = BukkitHelperDBDir

import Events
import Player
Player.BukkitHelperDir = BukkitHelperDir
Player.BukkitHelperDBDir = BukkitHelperDBDir
Player.Server = Server
#Player.Block = Block
import Console
import TaskManager

#Block.Server = Server

import goodshelve

DB = goodshelve.open(os.path.join(BukkitHelperDBDir, "DB.bin"))

def OnShutdown():
    if not isShutdown:
        Player.isOnline = False
        Player.OnShutdown()
        TaskManager.OnShutdown()
        DB.close()
