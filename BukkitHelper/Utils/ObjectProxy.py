# -*- coding: utf-8 -*-


class ObjectProxy:

    class _proxyCallback:
        def __init__(self, callbackObject, methodname):
            self.callbackObject = callbackObject
            self.methodname = methodname

        def _call(self, *args, **kargs):
            method = getattr(self.callbackObject, self.methodname)
            return method(*args, **kargs)

    def __init__(self, proxyObject):
        for method in dir(proxyObject):
            if method in self.__dict__:
                continue
            callBack = ObjectProxy._proxyCallback(proxyObject, method)
            self.__dict__[method] = callBack._call


class DynamicObjectProxy:

    class _proxyCallback:
        def __init__(self, callbackProxy, methodname):
            self.callbackProxy = callbackProxy
            self.methodname = methodname

        def _call(self, *args, **kargs):
            return self.callbackProxy._callBack_(
                    self.methodname, *args, **kargs
                )

    def __init__(self):
        self._wrappedMethods_ = []
        self.updateDict()

    def _callBack_(self, methodname, *args, **kargs):
        destinationObject = self.getDestinationObject()
        method = getattr(destinationObject, methodname)
        return method(*args, **kargs)

    def updateDict(self):
        for method in self._wrappedMethods_:
            del self.__dict__[method]
        self._wrappedMethods_ = []
        proxyObject = self.getDestinationObject()
        for method in dir(proxyObject):
            if method in self.__dict__:
                continue
            callBack = DynamicObjectProxy._proxyCallback(self, method)
            self.__dict__[method] = callBack._call
            self._wrappedMethods_.append(method)

    def getDestinationObject(self):
        raise NotImplementedError("Please Implement this method")


if __name__ == "__main__":
    import java.lang.String as jstr

    class jstrproxy(DynamicObjectProxy):

        def __init__(self, string):
            self.oo = string
            DynamicObjectProxy.__init__(self)

        def getDestinationObject(self):
            return self.oo

        def setNewObject(self, o):
            self.oo = o
            self.updateDict()

    testObject = jstrproxy(jstr("hallo"))

    import code
    shell = code.InteractiveConsole(globals())
    shell.interact()