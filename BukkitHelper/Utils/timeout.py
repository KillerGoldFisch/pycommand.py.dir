# -*- coding: utf-8 -*-
#http://stackoverflow.com/questions/2281850/timeout-function-if-it-takes-too-long-to-finish

from functools import wraps
import errno
import os
import signal
import time


class TimeoutError(Exception):
    pass


def timeout(seconds=10, error_message=os.strerror(errno.ETIME)):
    def decorator(func):
        def _handle_timeout(signum, frame):
            raise TimeoutError(error_message)

        def wrapper(*args, **kwargs):
            signal.signal(signal.SIGALRM, _handle_timeout)
            signal.alarm(seconds)
            try:
                result = func(*args, **kwargs)
            finally:
                signal.alarm(0)
            return result

        return wraps(func)(wrapper)

    return decorator

def callTimeout(seconds=10, func=None, *args, **kargs):
    """
    >>> callTimeout(5, lambda: time.sleep(4))
    >>> callTimeout(5, lambda: time.sleep(6))
    Traceback (most recent call last):
        ...
    TimeoutError: Timer expired
    """
    @timeout(seconds)
    def _call(func, *args, **kargs):
        return func(*args, **kargs)
    return _call(func, *args, **kargs)


if __name__ == "__main__":

    import doctest
    doctest.testmod()
    """
    import time
    @timeout(5)
    def test1(args):
        print "test1 in: " + args
        time.sleep(4)
        print "test1 out: " + args
    print "Run test1 (No Timeout)"
    test1("test1")

    @timeout(5)
    def test2(args):
        print "test2 in: " + args
        time.sleep(6)
        print "test2 out: " + args
    print "Run test2 (Timeout)"
    #test2("test2")

    def test3(args):
        print "test3 in: " + args
        time.sleep(6)
        print "test3 out: " + args
    test3 = timeout(5)(test3)
    print "Run test2 (Timeout)"
    #test3("test3")

    callTimeout(5, lambda: time.sleep(4))
    callTimeout(5, lambda: time.sleep(6))
    """