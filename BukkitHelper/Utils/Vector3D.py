#!/usr/bin/env python
#http://docs.python.org/release/2.5.2/ref/numeric-types.html


import math


def _isVector3D(obj):
    if type(obj).__name__=='instance':
        if obj.__class__.__name__=='Vector3D':
            return True
    return False


class Vector3D:

    def __init__(self,x=0.0,y=0.0,z=0.0):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)

    def __add__(self, other):	# on self + other
        if _isVector3D(other):
            return Vector3D(self.x + other.x, self.y + other.y, self.z + other.z)
        else:
            return self.len() + other

    def __sub__(self, other):	# on self - other
        if _isVector3D(other):
            return Vector3D(self.x - other.x, self.y - other.y, self.z - other.z)
        else:
            return self.len() - other

    def __mul__(self, other):	#on self * other
        if _isVector3D(other):
            return Vector3D(self.x * other.x, self.y * other.y, self.z * other.z)
        else:
            return Vector3D(self.x * other, self.y * other, self.z * other)


    def __div__(self, other):   #on self / other
        if _isVector3D(other):
            return Vector3D(self.x / other.x, self.y / other.y, self.z / other.z)
        else:
            return Vector3D(self.x / other, self.y / other, self.z / other)



    def __iadd__(self, other):  #on self += other
        if _isVector3D(other):
            self.x += other.x
            self.y += other.y
            self.z += other.z
        else:
            self.x += other
            self.y += other
            self.z += other
        return self


    def __isub__(self, other):  #on self += other
        if _isVector3D(other):
            self.x -= other.x
            self.y -= other.y
            self.z -= other.z
        else:
            self.x -= other
            self.y -= other
            self.z -= other
        return self


    def __imul__(self, other):	#on self * other
        if _isVector3D(other):
            self.x *= other.x
            self.y *= other.y
            self.z *= other.z
        else:
            self.x *= other
            self.y *= other
            self.z *= other
        return self

    def __idiv__(self, other):   #on self / other
        if _isVector3D(other):
            self.x /= other.x
            self.y /= other.y
            self.z /= other.z
        else:
            self.x /= other
            self.y /= other
            self.z /= other
        return self


    def __cmp__(self, other):   #on compare other
        self_len = self.len()
        other_len = 0.0
        if _isVector3D(other):
            other_len = other.len()
        else:
            other_len = other

        if self_len == other_len: return 0
        if self_len < other_len: return -1
        if self_len > other_len: return 1

    def __str__( self ):
        return self.toString()

    def toString(self,roundn = 3):
        return "[x="+str(round(self.x,roundn))+", y="+str(round(self.y,roundn))+", z="+str(round(self.z,roundn))+"]"

    	# overload []
    def __getitem__(self, index):
        if index == 0:
            return self.x
        elif index == 1:
            return self.y
        elif index == 2:
            return self.z
        raise Error("out of index!")

    # overload set []
    def __setitem__(self, key, item):
            print "__setitem__"
            self.data[key] = item

    def cross(self, other):
        a = self
        b = other
        return Vector3D(
            a.y*b.z - a.z*b.y,
            a.z*b.x - a.x*b.z,
            a.x*b.y - a.y*b.x
        )

    def get(self):
        return self.x, self.y, self.z


    def len(self):
        return math.sqrt(self.x**2 + self.y**2 + self.z**2)

    def unit(self):
        return self / self.len()


    def rotateX(self, theta):
        newv = Vector3D()
        newv.x = self.x
        newv.y = self.y * math.cos(theta) + self.z * -math.sin(theta)
        newv.z = self.y * math.sin(theta) + self.z * math.cos(theta)
        return newv

    def rotateY(self, theta):
        newv = Vector3D()
        newv.x = self.x * math.cos(theta) + self.z * math.sin(theta)
        newv.y = self.y
        newv.z = self.z * -math.sin(theta) + self.z * math.cos(theta)
        return newv

    def rotateZ(self, theta):
        newv = Vector3D()
        newv.x = self.x * math.cos(theta) + self.y * -math.sin(theta)
        newv.y = self.x * math.sin(theta) + self.y * math.cos(theta)
        newv.z = self.z
        return newv


    def rotateAroundVector(self, vect2, theta):
        newv = Vector3D()
        unit = self.unit()

        q0 = math.cos(theta/2.0);
        q1 = math.sin(theta/2.0)*unit.x
        q2 = math.sin(theta/2.0)*unit.y
        q3 = math.sin(theta/2.0)*unit.z

        # column vect
        newv.x =   (q0*q0 + q1*q1 - q2*q2 - q3*q3)*self.x +               2.0*(q2*q1 - q0*q3) * self.y +               2.0*(q3*q1 + q0*q2) * self.z
        newv.y =               2.0*(q1*q2 + q0*q3)*self.x +   (q0*q0 - q1*q1 + q2*q2 - q3*q3) * self.y +               2.0*(q3*q2 - q0*q1) * self.z
        newv.z =               2.0*(q1*q3 - q0*q2)*self.x +               2.0*(q2*q3 + q0*q1) * self.y +   (q0*q0 - q1*q1 - q2*q2 + q3*q3) * self.z
        return newv

    def rotateGL(self, i = 0.0, j = 0.0, k = 0.0):
        newv = Vector3D()
        vy = Vector3D(y=1.0)
        vz = Vector3D(z=1.0)

        newv = self.rotateX(i)
        vy = vy.rotateX(i)
        vz = vz.rotateX(i)

        newv = newv.rotateAroundVector(vy, j)
        vz = vz.rotateAroundVector(vy, j)

        return newv.rotateAroundVector(vz, k)


NULL = Vector3D()
X = Vector3D(1.0, 0, 0)
Y = Vector3D(0, 1.0, 0)
Z = Vector3D(0, 0, 1.0)