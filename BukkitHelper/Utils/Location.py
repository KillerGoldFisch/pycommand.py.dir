# -*- coding: utf-8 -*-

import ObjectProxy

import org.bukkit as Bukkit
Server = Bukkit.Bukkit.getServer()

__fastmode__ = True

class Location(ObjectProxy.DynamicObjectProxy):

    def __init__(self, bukkitLocation):
        if __fastmode__:
            self.__blocation = bukkitLocation
        self.World = bukkitLocation.getWorld().getName()
        self.X = bukkitLocation.getX()
        self.Y = bukkitLocation.getY()
        self.Z = bukkitLocation.getZ()
        self.Yaw = bukkitLocation.getYaw()
        self.Pitch = bukkitLocation.getPitch()

        ObjectProxy.DynamicObjectProxy.__init__(self)

    @classmethod
    def fromCoord(cls, World, X, Y, Z, Yaw=0.0, Pitch=0.0):
        import org.bukkit
        world = Server.getWorld(World)
        loc = org.bukkit.Location(world, X, Y, Z, Yaw, Pitch)
        return cls(loc)


    def getDestinationObject(self):
        if __fastmode__:
            return self.__blocation
        import org.bukkit
        world = Server.getWorld(self.World)
        return org.bukkit.Location(
            world, self.X, self.Y, self.Z, self.Yaw, self.Pitch)

    def __str__(self):
        return \
            '[Location: World="%s" X="%s" Y="%s" Z="%s" Yaw="%s" Pitch="%s"]' \
            % (
                self.World, self.X, self.Y, self.Z, self.Yaw, self.Pitch
            )

    def getBukkitLocation(self):
        return self.getDestinationObject()

    def getXYZ(self):
        return (self.X, self.Y, self.Z)

    def getDistance(self, otherLocation):
        if self.World != otherLocation.World:
            raise Exception("Locations in in different Worlds")

        import math

        return math.sqrt((self.X - otherLocation.X) ** 2 +
                        (self.Y - otherLocation.Y) ** 2 +
                        (self.Z - otherLocation.Z) ** 2)

    def getVector3D(self):
        import Vector3D
        return Vector3D.Vector3D(self.X, self.Y, self.Z)
