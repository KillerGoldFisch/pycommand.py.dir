
import thread, threading, time, KThread

GlobalLock = thread.allocate_lock()

GThreads = []


class GThread:

    def __init__(self):
        class _thread(KThread.KThread):
            def __init__(self, gthread):
                self._gthread = gthread
                KThread.KThread.__init__(self)
            def run(self):
                for onStart in self._gthread.OnStart:
                    onStart(self._gthread)
                GThreads.append(self._gthread)
                self._gthread.isRunning = True
                try:
                    self._gthread.run(*self._gthread._args, **self._gthread._kargs)
                except Exception, ex:
                    print str(ex)
                self._gthread.isRunning = False
                GThreads.remove(self._gthread)
                for onEnd in self._gthread.OnEnd:
                    onEnd(self._gthread)
        self.isRunning = False
        self.Thread = _thread(self)
        self.Tags = {}
        self.OnStart = []
        self.OnEnd = []
        self.Name = "NONAME"

    @classmethod
    def DirectStart(cls, function, *args, **kargs):
        class _gthread(GThread):
            def run(self, *args, **kargs):
                function(*args, **kargs)
        gthread = _gthread()
        gthread.setDaemon(True)
        gthread.start(*args, **kargs)
        return gthread

    def start(self, *args, **kargs):
        self._args = args
        self._kargs = kargs
        self.Thread.start()

    def sleep(self, secounds):
        time.sleep(secounds)

    def join(self, timeout=None):
        self.Thread.join(timeout)

    def timeout(self, timeout):
        self.join(timeout)
        if self.Thread.isAlive():
            self.kill()
            raise Exception("Timeout on Thread '%s'"%self.Name)

    def kill(self):
        self.Thread.kill()
        #self.Thread.terminate()
        self.isRunning = False
        if self in GThreads:
            GThreads.remove(self)

    def setName(self, name="NONAME"):
        self.Name = name
        return self


class GThreadLimited(GThread):
    def __init__(self, maxUsage):
        self._maxUsage = maxUsage
        GThread.__init__(self)
        def _onStart(self):
            self._lastSleep = time.time()
        self._lastSleep = time.time()
        self.OnStart.append(_onStart)

    @classmethod
    def DirectStart(cls, function, maxUsage, *args, **kargs):
        #print "GThreadLimited.DirectStart(function=%s, maxUsage=%f)"%(
        #    str(function), maxUsage)
        class _gthread(GThreadLimited):
            def run(self, *args, **kargs):
                #print "run(function='%s', *args='%s', **kargs='%s'"%(
                #    str(function),
                #    args,
                #    kargs)
                function(*args, **kargs)
        gthread = _gthread(maxUsage)
        gthread.start(*args, **kargs)
        return gthread

    def sleep(self, secounds):
        now = time.time()
        timeinuse = now - self._lastSleep
        timetosleep = timeinuse / self._maxUsage
        if timetosleep < secounds:
            timetosleep = secounds
        #print "Used=%f Sleep=%f"%(timeinuse, timetosleep)
        time.sleep(timetosleep)
        self._lastSleep = time.time()


def CurrentGThread():
    """
    @rtype:   GThread
    @return:  the current GThread wich is running
    """
    t = threading.currentThread()
    for gthread in GThreads:
        if gthread.Thread is t:
            return gthread
    return None


def sleep(secounds):
    gt = CurrentGThread()
    if gt is not None:
        gt.sleep(secounds)
    else:
        time.sleep(secounds)



if __name__ == "__main__":
    def _test(*args, **kargs):
        print "arg="+str(args) + " kargs="+str(kargs)
        sleep(1.5)
    #GThread.DirectStart(_test, "test", test=42).setName("Test Thread").timeout(1)

    def getPrims(max_n):
        #print "max_n="+str(max_n)
        # http://stackoverflow.com/questions/2068372/fastest-way-to-list-all-primes-below-n-in-python/2073279#2073279
        numbers = range(3, max_n+1, 2)
        half = (max_n)//2
        initial = 4

        for step in xrange(3, max_n+1, 2):
            for i in xrange(initial, half, step):
                numbers[i-1] = 0
            initial += 2*(step+1)

            if initial > half:
                return [2] + filter(None, numbers)
    def _test(*args, **kargs):
        #print "arg="+str(args) + " kargs="+str(kargs)
        for n in range(1000):
            print "Test " + str(n)
            getPrims((n + 1) * 10000)
            sleep(0)
    try:
        GThreadLimited.DirectStart(_test, 0.3, "test", test=42).setName("Test Thread").timeout(20)
    except:
        pass

    raw_input("ende")

    #import code
    #shell = code.InteractiveConsole(globals())
    #shell.interact()


