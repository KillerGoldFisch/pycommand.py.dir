# -*- coding: utf-8 -*-
import ObjectProxy, itertools
import GThread


class GeneratorProxy(ObjectProxy.DynamicObjectProxy):

    def __init__(self, callbackType, generator):
        self._callBackType_ = callbackType
        self._generator_ = generator
        ObjectProxy.DynamicObjectProxy.__init__(self)

    def _callBack_(self, methodname, *args, **kargs):
        returnList = []
        #self._generator_, generator = itertools.tee(self._generator_)
        generator = self._generator_
        n = 0
        for obj in generator:
            n += 1
            if n % 10 == 0:
                GThread.sleep(0.2)
            method = getattr(obj, methodname)
            try:
                returnList.append(method(*args, **kargs))
            except Exception, e:
                returnList.append(e)
        return returnList

    def getDestinationObject(self):
        return self._callBackType_

    def __iter__(self):
        return self

    def next(self):
        for n in self._generator_:
            return n
        raise StopIteration

    def __next__(self):
        for n in self._generator_:
            return n
        raise StopIteration

    def getGeneratorCopy(self):
        self._generator_, generator = itertools.tee(self._generator_)
        return generator


if __name__ == "__main__":
    def _g():
        yield "asdf"
        yield "yxcv"
    gp = GeneratorProxy(str, _g())

    import code
    shell = code.InteractiveConsole(globals())
    shell.interact()
