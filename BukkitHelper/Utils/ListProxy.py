# -*- coding: utf-8 -*-


class ListProxy(list):

    class _proxyCallback:
        def __init__(self, callbacklist, methodname):
            self.callbacklist = callbacklist
            self.methodname = methodname

        def _call(self, *args, **kargs):
            return self.callbacklist._callBack(self.methodname, *args, **kargs)

    def __init__(self, listtype, *args, **kargs):
        self.ListType = listtype
        #self.extend(lst)
        for method in dir(listtype):
            if method in self.__dict__:
                continue
            callBack = ListProxy._proxyCallback(self, method)
            self.__dict__[method] = callBack._call
        super(ListProxy, self).__init__(*args, **kargs)

    def _callBack(self, methodname, *args, **kargs):
        #print methodname + "Callback with " + str(args) + " : " + str(kargs)
        returndict = {}
        import time
        n = 0
        for item in self:
            n += 1
            if n % 200 == 0:
                time.sleep(0.333)
            try:
                method = getattr(item, methodname)
                returndict[item] = method(*args, **kargs)
            except Exception, e:
                returndict[item] = e
        return returndict

    def __getslice__(self, i, j):
        return ListProxy(self.ListType, list.__getslice__(self, i, j))

    def clearList(self):
        del self[0:len(self)]

###############################################################################

if __name__ == "__main__":
    import code
    shell = code.InteractiveConsole(globals())
    shell.interact()