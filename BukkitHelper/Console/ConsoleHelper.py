# -*- coding: utf-8 -*-


def getScriptByName(name):
    import os
    scriptpath = os.path.abspath(os.path.dirname(__file__) + "/../scripts/")
    scriptfile = os.path.join(scriptpath, name + ".py")
    print "Run Script: " + scriptfile
    if os.path.exists(scriptfile):
        return scriptfile
    return None
