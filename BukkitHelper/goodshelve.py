import types

def getBuiltinDict():
    import __builtin__
    builtins = {}
    for key in dir(__builtin__):
        builtins[key] = getattr(__builtin__, key)
    return builtins

class Shelf:
    """Base class for shelf implementations.

    This is initialized with a dictionary-like object.
    See the module's __doc__ string for an overview of the interface.
    """

    def __init__(self, shelve):
        self.shelve = shelve
        self.dict = getBuiltinDict()

    def keys(self):
        return self.dict.keys() + self.shelve.keys()

    def update(self, dictionary):
        self.dict.update(dictionary)

    def __len__(self):
        return len(self.dict) + len(self.shelve)

    def has_key(self, key):
        return self.dict.has_key(key) or self.shelve.has_key(key)

    def get(self, key, default=None):
        if self.dict.has_key(key):
            return self.dict[key]
        if self.shelve.has_key(key):
            return self.shelve[key]
        return default

    def __getitem__(self, key):
        if self.dict.has_key(key):
            #print "goodshelve.__getitem__(key='%s', value='%s') -> dict"%(key, self.dict[key])
            return self.dict[key]
        if self.shelve.has_key(key):
            #print "goodshelve.__getitem__(key='%s', value='%s') -> shelve"%(key, self.shelve[key])
            return self.shelve[key]
        #print "goodshelve.__getitem__(key='%s', value='%s') -> not found"%(key, None)

    def __setitem__(self, key, value):
        if isinstance(value, (
                types.TypeType,
                types.FunctionType,
                types.ModuleType,
                types.BuiltinFunctionType,
                types.FileType,
                types.FrameType,
                types.GeneratorType,
                types.NotImplementedType,
                types.UnboundMethodType,
                )):
            #print "goodshelve.__setitem__(key='%s', value='%s') -> dict"%(key, value)
            self.dict[key] = value
        else:
            try:
                self.shelve[key] = value
                #print "goodshelve.__setitem__(key='%s', value='%s') -> shelve"%(key, value)
            except:
                self.dict[key] = value
                #print "goodshelve.__setitem__(key='%s', value='%s') -> dict"%(key, value)

    def __delitem__(self, key):
        try:
            del self.shelve[key]
        except:
            del self.dict[key]

    def close(self):
        try:
            self.shelve.close()
        except:
            pass
        self.dict = 0

    def __del__(self):
        self.close()

    def sync(self):
        self.shelve.sync()

def open(filename, flag='c'):
    import shelve
    return Shelf(shelve.open(filename, flag))