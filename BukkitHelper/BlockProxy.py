# -*- coding: utf-8 -*-

import Utils.ObjectProxy as ObjectProxy
import Utils.Location as Location
import Utils.GeneratorProxy as GeneratorProxy
import Utils.GThread

import org.bukkit as Bukkit
Server = Bukkit.Bukkit.getServer()

_blockCache = {}

__fastmode__ = True


class Block(ObjectProxy.DynamicObjectProxy):

    def __init__(self, bukkitblock):
        if __fastmode__:
            self._bblock = bukkitblock
        self.Location = Location.Location(bukkitblock.getLocation())
        self._typeId = bukkitblock.getTypeId()
        self._data = bukkitblock.getData()
        #_blockCache[bukkitblock] = self #TODO Testen ob es was aus macht
        ObjectProxy.DynamicObjectProxy.__init__(self)


    @classmethod
    def fromCoord(cls, World, X, Y, Z, Yaw=0.0, Pitch=0.0):
        import org.bukkit
        world = Server.getWorld(World)
        loc = org.bukkit.Location(world, X, Y, Z, Yaw, Pitch)
        bukkitblock = loc.getBlock()
        return Block.fromBBlock(bukkitblock)

    @classmethod
    def fromBLocation(cls, loc):
        bukkitblock = loc.getBlock()
        return Block.fromBBlock(bukkitblock)

    @classmethod
    def fromBBlock(cls, bukkitblock):
        if bukkitblock in _blockCache:
            return _blockCache[bukkitblock]
        return cls(bukkitblock)

    @classmethod
    def fromTuple(cls, tup):
        world, x, y, z, Id, data = tup
        block = Block.fromCoord(world, x, y, z)
        block._typeId = Id
        block._data = data
        return block

    def getDestinationObject(self):
        if __fastmode__:
            return self._bblock
        return self.Location.getBukkitLocation().getBlock()

    def saveState(self):
        bukkitBlock = self.getDestinationObject()
        self._typeId = bukkitBlock.getTypeId()
        self._data = bukkitBlock.getData()

    def restoreState(self):
        bukkitBlock = self.getDestinationObject()
        bukkitBlock.setTypeId(self._typeId)
        bukkitBlock.setData(self._data)

    def setOnFire(self):
        loc = self.Location.getBukkitLocation()
        loc = loc.add(0.0, 1.0, 0.0)
        upperBlock = loc.getBlock()
        if upperBlock.isEmpty():
            upperBlock.setTypeIdAndData(51, 16, True)

    def explode(self, radius=5.0, fire=False):
        bblock = self.getDestinationObject()
        bblock.getWorld().createExplosion(bblock.getLocation(), radius, fire)

    def paste(self, slot=0):
        if isinstance(slot, str):
            import pickle
            pkl_file = open(BukkitHelperDBDir + "/copy_" + slot, 'rb')
            from_origin, from_BlockList = pickle.load(pkl_file)
            pkl_file.close()
            from_origin = Block.fromTuple(from_origin)
            from_BlockList = BlockList.fromTupleList(from_BlockList)
        else:
            from_origin, from_BlockList = _copySlots[slot]
        vec_from = from_origin.Location.getVector3D()
        vec_to = self.Location.getVector3D()
        vec_delta = vec_to - vec_from
        for block in from_BlockList:
            vec_destination = block.Location.getVector3D() + vec_delta
            block_destination = Block.fromCoord(self.Location.World,
                vec_destination.x, vec_destination.y, vec_destination.z)
            bblock_destination = block_destination.getDestinationObject()
            bblock_source = block.getDestinationObject()
            btype = bblock_source.getTypeId()
            bdata = bblock_source.getData()
            bblock_destination.setTypeIdAndData(btype, bdata, False)

    def toTuple(self):
        return (self.Location.World,
                self.Location.X, self.Location.Y, self.Location.Z,
                self._typeId, self._data)

    def move(self, x=0.0, y=0.0, z=0.0):
        #self.saveState()
        self.setTypeId(0)
        self.Location.X += x
        self.Location.Y += y
        self.Location.Z += z
        self.restoreState()

    def relative(self, x=0.0, y=0.0, z=0.0):
        return Block.fromCoord(self.Location.World,
            self.Location.X + x,
            self.Location.Y + y,
            self.Location.Z + z)

    def getSphere(self, radius=10):
        def _getSphere(radius):
            block0 = self
            center = block0.Location
            x_span = (block0.Location.X - radius, block0.Location.X + radius)
            y_span = (block0.Location.Y - radius, block0.Location.Y + radius)
            z_span = (block0.Location.Z - radius, block0.Location.Z + radius)

            for x in range(x_span[0], x_span[1] + 1):
                Utils.GThread.sleep(0.1)
                for y in range(y_span[0], y_span[1] + 1):
                    for z in range(z_span[0], z_span[1] + 1):
                        loc = Location.Location.fromCoord(center.World, x, y, z)
                        if center.getDistance(loc) <= radius:
                            yield Block.fromCoord(center.World, x, y, z)
        return BlockGenerator( _getSphere(radius))


import Utils.ListProxy

_copySlots = {}


class BlockList(Utils.ListProxy.ListProxy):

    def __init__(self, *args, **kargs):
        import org.bukkit.block
        self._origin = None
        Utils.ListProxy.ListProxy.__init__(
            self,
            org.bukkit.block.Block, *args, **kargs
            )

    def __getslice__(self, i, j):
        return BlockList(self.ListType, list.__getslice__(self, i, j))

    @classmethod
    def fromTupleList(cls, tupleList):
        blockList = cls()
        for tup in tupleList:
            blockList.append(Block.fromTuple(tup))
        return blockList

    def saveState(self):
        for block in self:
            block.saveState()

    def restoreState(self):
        for block in self:
            block.restoreState()

    def setOnFire(self):
        for block in self:
            block.setOnFire()

    def move(self, x=0.0, y=0.0, z=0.0):
        n = 0
        for block in self:
            n += 1
            if n % 100 == 0:
                Utils.GThread.sleep(0.1)
            block.saveState()
            block.setTypeId(0)
        n = 0
        for block in self:
            n += 1
            if n % 100 == 0:
                Utils.GThread.sleep(0.1)
            block.Location.X += x
            block.Location.Y += y
            block.Location.Z += z
            block.restoreState()

    def copy(self, slot=0, origin=None):
        if origin is None:
            origin = self._origin
        if origin is None:
            origin = self[-1]
        if isinstance(slot, str):
            import pickle
            output_filename = BukkitHelperDBDir + "/copy_" + slot
            print "Write to " + output_filename
            output = open(output_filename, 'wb')
            pickle.dump((origin.toTuple(), self.toTupleList()), output)
            output.close()
        else:
            _copySlots[slot] = (origin, self)

    def filter(self, Ids=[0, ], data=None, callback=None):
        def _filter(Ids, data, callback):
            if not isinstance(Ids, (list, tuple)):
                Ids = [Ids, ]
            if data is not None:
                if not isinstance(data, (list, tuple)):
                    data = [data, ]

            n = 0

            for block in self:
                n += 1
                if n % 100 == 0:
                    Utils.GThread.sleep(0.1)
                #Check for Type IDs
                if not block.getTypeId() in Ids:
                    continue
                #Check for Block Data
                if data is not None:
                    if not block.getData() in data:
                        continue
                #check for Callback
                if callback is not None:
                    if not callback(block):
                        continue
                yield block
        return BlockGenerator( _filter(Ids, data, callback))

    def toTupleList(self):
        returnList = []
        for block in self:
            returnList.append(block.toTuple())
        return returnList

    def replace(self, fromTypesId, toTypeId):
        if not isinstance(fromTypesId, (list, tuple)):
            fromTypesId = [fromTypesId, ]
        n = 0
        for block in self:
            n += 1
            if n % 100 == 0:
                Utils.GThread.sleep(0.1)
            typeId = block.getTypeId()
            if typeId in fromTypesId:
                block.setTypeId(toTypeId)

    def getBox(self, hollow=False, block0=None, block1=None):
        def _getBox(hollow, block0, block1):
            if block0 is None:
                block0 = self[-1]
            if block1 is None:
                block1 = self[-2]
            world = block0.Location.World
            x_coords = [block0.Location.X, block1.Location.X]
            y_coords = [block0.Location.Y, block1.Location.Y]
            z_coords = [block0.Location.Z, block1.Location.Z]

            x_span = (min(x_coords), max(x_coords))
            y_span = (min(y_coords), max(y_coords))
            z_span = (min(z_coords), max(z_coords))

            ammount = (x_span[1] - x_span[0]) * \
                      (y_span[1] - y_span[0]) * \
                      (z_span[1] - z_span[0])

            #if ammount > 6666:
            #    raise Exception('To many Blocks: ' + str(ammount))

            if hollow:
                #Wall 1 & 3
                for x in range(x_span[0], x_span[1] + 1):
                    for y in range(y_span[0], y_span[1] + 1):
                        yield Block.fromCoord(world, x, y, z_span[0])
                        yield Block.fromCoord(world, x, y, z_span[1])
                Utils.GThread.sleep(0.1)
                #Wall 2 & 4
                for y in range(y_span[0], y_span[1] + 1):
                    for z in range(z_span[0], z_span[1] + 1):
                            yield Block.fromCoord(world, x_span[0], y, z)
                            yield Block.fromCoord(world, x_span[1], y, z)
                Utils.GThread.sleep(0.1)
                #Roof & Floor
                for x in range(x_span[0], x_span[1] + 1):
                    for z in range(z_span[0], z_span[1] + 1):
                            yield Block.fromCoord(world, x, y_span[0], z)
                            yield Block.fromCoord(world, x, y_span[1], z)
            else:

                for x in range(x_span[0], x_span[1] + 1):
                    Utils.GThread.sleep(0.1)
                    for y in range(y_span[0], y_span[1] + 1):
                        for z in range(z_span[0], z_span[1] + 1):
                            yield Block.fromCoord(world, x, y, z)

        generator = BlockGenerator(_getBox(hollow, block0, block1))
        generator._origin = block1
        return generator

    def getWall(self, block0=None, block1=None):
        def _getWall(block0, block1):
            if block0 is None:
                block0 = self[-1]
            if block1 is None:
                block1 = self[-2]
            world = block0.Location.World
            x_coords = [block0.Location.X, block1.Location.X]
            y_coords = [block0.Location.Y, block1.Location.Y]
            z_coords = [block0.Location.Z, block1.Location.Z]

            x_span = (min(x_coords), max(x_coords))
            y_span = (min(y_coords), max(y_coords))
            z_span = (min(z_coords), max(z_coords))

            ammount = (x_span[1] - x_span[0]) * \
                      (y_span[1] - y_span[0]) * \
                      (z_span[1] - z_span[0])

            #if ammount > 6666:
            #    raise Exception('To many Blocks: ' + str(ammount))


            #Wall 1 & 3
            for x in range(x_span[0], x_span[1] + 1):
                for y in range(y_span[0], y_span[1] + 1):
                    yield Block.fromCoord(world, x, y, z_span[0])
                    yield Block.fromCoord(world, x, y, z_span[1])
            Utils.GThread.sleep(0.1)
            #Wall 2 & 4
            for y in range(y_span[0], y_span[1] + 1):
                for z in range(z_span[0], z_span[1] + 1):
                        yield Block.fromCoord(world, x_span[0], y, z)
                        yield Block.fromCoord(world, x_span[1], y, z)

        generator = BlockGenerator(_getWall(block0, block1))
        generator._origin = block1
        return generator

    def getSphere(self, radius=10, block0=None):
        if block0 is None:
            block0 = self[-1]
        return block0.getSphere(radius)


class BlockGenerator(GeneratorProxy.GeneratorProxy):

    def __init__(self, generator):
        import org.bukkit.block
        GeneratorProxy.GeneratorProxy.__init__(self,
            org.bukkit.block.Block, generator)

    def filter(self, Ids=[0, ], data=None, callback=None):
        def _filter(Ids, data, callback):
            if not isinstance(Ids, (list, tuple)):
                Ids = [Ids, ]
            if data is not None:
                if not isinstance(data, (list, tuple)):
                    data = [data, ]

            n = 0

            for block in self._generator_:
                n += 1
                if n % 100 == 0:
                    Utils.GThread.sleep(0.1)
                #Check for Type IDs
                if not block.getTypeId() in Ids:
                    continue
                #Check for Block Data
                if data is not None:
                    if not block.getData() in data:
                        continue
                #check for Callback
                if callback is not None:
                    if not callback(block):
                        continue
                yield block

        generator = BlockGenerator(_filter(Ids, data, callback))
        return generator
