import thread, threading
import time

def OnShutdown():
    for task in Task._tastks.values():
        task.Running = False

class Task:
    
    _tastks = {}
    
    @staticmethod
    def getCurrenTask():
        id = thread.get_ident()
        if Task._tastks.has_key(id):
            return Task._tastks[id]
        else:
            return None
    
    @staticmethod
    def getCurrenThread():
        threading.currentThread()
    
    @staticmethod
    def ResetWatchdog():
        task = Task.getCurrenTask()
        if task != None:
            task.LastWatchdog = time.time()
    
    def __init__(self, function, args = (), trigger = 1.0, 
                 watchdog = True, watchdogtimeout = 5.0, watchdoghandler = None,
                 taskname = "Noname"):
        self.Name = taskname
        self.ThreadId = 0
        self.TriggerTime = trigger
        self.LastWatchdog = time.time()
        self._watchdogHandler = watchdoghandler
        self.WatchdogTimeout = watchdogtimeout
        self.Running = True
        self.Thread = None
        self.Exited = False
        self._args = args
        def _starter(self):
            self.Thread = threading.currentThread()
            self.ThreadId = thread.get_ident()
            Task._tastks[self.ThreadId] = self
            last_time = time.time()
            while self.Running:
                timeToSleep = last_time - time.time() + self.TriggerTime
                timeToSleep = timeToSleep if timeToSleep > 0.0 else 0.0
                time.sleep(timeToSleep)
                last_time = time.time()
                Task.ResetWatchdog()
                #print "%s - %s"%(time.time(), timeToSleep)
                try:
                    function(*(self._args))
                except:
                    pass
            self.Exited = True
        thread.start_new_thread(_starter, (self,))
                
    def End(self):
        self.Running = False
        stoptime = time.time() + 0.6
        while not self.Exited:
            time.sleep(0.01)
            if time.time() > stoptime:
                self.Kill()
                return False
        return True
    
    def Kill(self):
        self.Running = False
        print dir(self.Thread)
        print type(self.Thread)
        try:
            self.Thread._Thread__stop()
        except:
            pass
        
def _watchdogLoop():
    for task in Task._tastks.values():
        if task.Running:
            if task.LastWatchdog + task.WatchdogTimeout < time.time():
                if task._watchdogHandler != None:
                    task._watchdogHandler(task)
                print "WD : " + task.Name
                task.Kill()

WatchdogTask = Task(_watchdogLoop, taskname = "WatchDog-Task")
                  
if __name__ == "__main__":
    def _print(text):
        print text
    t1 = Task(lambda : _print("Task1 " +str(time.time())), taskname = "Task 1")
    def t2():
        time.sleep(0.5)
        print "Task 2"+str(time.time())
    t2 = Task(t2, taskname = "Task 2")
    def t3():
        time.sleep(10.0)
        print "Task 3 #####################"
    t3 = Task(lambda : time.sleep(10.0), taskname = "Task 3")
    time.sleep(30)
    
    print t1.End()
    print t2.End()
    print t3.End()
    
    time.sleep(5)
                      