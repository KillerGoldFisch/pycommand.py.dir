# -*- coding: utf-8 -*-
import traceback

from org.bukkit.event import EventPriority
from org.bukkit.event.player import PlayerInteractEvent

__plugin_name__ = "PyCommandPlugin"
__plugin_version__ = "0.1"
__plugin_mainclass__ = "PyCommandPlugin"

CommandPrefix = "py"

sys.path.append("./plugins/PyCommand.py.dir")
import BukkitHelper
from TelnetRepl import TelnetRepl
from TelnetRepl import ConsoleClientPlus

ConsoleConsole = BukkitHelper.Console.customConsoleClass(BukkitHelper.DB)
ConsoleConsole.push("import BukkitHelper")

CommandBlockConsoleDict = {}

from java.util.logging import Level
log = server.getLogger()
CHAT_PREFIX = "[%s] " % __plugin_name__

def info(*text):
    log.log(Level.INFO,CHAT_PREFIX+" ".join(map(unicode,text)))

def severe(*text):
    log.log(Level.SEVERE,CHAT_PREFIX+" ".join(map(unicode,text)))

def msg(recipient,*text):
    recipient.sendMessage(CHAT_PREFIX+" ".join(map(unicode,text)))

class Listener(PythonListener):
    def __init__(self, plugin):
        self.plugin = plugin
        PythonListener.__init__(self)

    @PythonEventHandler(PlayerInteractEvent, EventPriority.NORMAL)
    def onPlayerInteract(self, event):
        self.plugin.onPlayerInteract(event)
        


class PyCommandPlugin(PythonPlugin):

    def __init__(self):
        info("PyCommand loaded")
        BukkitHelper.Share.Plugin = self
        super(PyCommandPlugin, self).__init__()

    def onEnable(self):
        pm = self.getServer().getPluginManager()
        self.listener = Listener(self)
        pm.registerEvents(self.listener, self)

        try:
            self.telnetRepl = TelnetRepl.TelnetRepl(auth=TelnetAuth(self))
        except Exception as ex:
            info(str(ex))
            info(traceback.format_exc())

        info("PyCommand enabled")

    def onDisable(self):
        print info("PyCommand disabled")
        self.telnetRepl.server.__exit__(None, None, None)

    def onCommand(self, sender, command, label, args):
        try:
            if label == "py":
                return self.onPyCommand(sender, args)
            elif label == "pytask":
                return self.onTaskCommand(sender, args)
            elif label == "pyrun":
                return self.onScriptCommand(sender, args)
        except Exception as ex:
            info(str(ex))
            info(traceback.format_exc())


    #@CommandHandler(CommandPrefix + "task" ,usage="/<command> [operation]",desc="Manage Python Tasks")
    def onTaskCommand(self, sender, args):
        name = sender.getName()

        console = _getConsole(sender)

        import BukkitHelper.Utils.GThread as gt

        if console is None:
            msg(sender, "You are not be able to do this, do you have the 'pycommand.shell' permission?")
            return False

        if len(args) == 0:
            return False

        if len(args) == 1:
            if args[0] == "ls":
                msg(sender,"\n".join(["    [%i] '%s'"%(n, gt.GThreads[n].Name) for n in range(len(gt.GThreads))]))
            else:
                return False
        elif len(args) == 2:
            if args[0] == "kill":
                if not args[1].isdigit():
                    return False
                taskNr = int(args[1])
                gt.GThreads[taskNr].kill()
            else:
                return False
        else:
            return False

        return True

    #@CommandHandler(CommandPrefix + "run" ,usage="/<command> [scriptname] [eval]",desc="run script")
    def onScriptCommand(self, sender, args):
        name = sender.getName()
        console = _getConsole(sender)

        if console is None:
            msg(sender, "You are not be able to do this, do you have the 'pycommand.shell' permission?")
            return False

        import BukkitHelper.Console.ConsoleHelper

        scriptfile = BukkitHelper.Console.ConsoleHelper.getScriptByName(args[0])
        if  scriptfile is None:
            msg(sender, args[0] + " not found")
            return False

        def _run():
            execfile(scriptfile, console.consoleLocals)

        import BukkitHelper.Utils.GThread
        import BukkitHelper
        timeout_time = BukkitHelper.DB.get("config.pytimeout", 60 * 5)

        if len(args) > 1:
            console.consoleLocals["pyrun_args"] = eval(" ".join(args[1:]), console.consoleLocals)
        else:
            console.consoleLocals["pyrun_args"] = None

        BukkitHelper.Utils.GThread.GThreadLimited \
            .DirectStart(_run, 0.3) \
            .setName("Script '%s' -> '%s'" % (name, args[0])) \
            .timeout(timeout_time)

        return True


    #@EventHandler
    def onPlayerInteract(self, playerInteractEvent):
        from org.bukkit.event.block import Action
        player = BukkitHelper.Player.getPlayer(playerInteractEvent.getPlayer())
        console = _getConsole(player.buk)
        if console is None:
            return
        itemStack = playerInteractEvent.getItem()
        if itemStack is None:
            return
        material = itemStack.getType()
        from org.bukkit import Material

        if material == Material.WOOD_AXE: ##########################################################
            if not playerInteractEvent.getAction() in \
                    [Action.RIGHT_CLICK_AIR, Action.RIGHT_CLICK_BLOCK,
                         Action.LEFT_CLICK_BLOCK, Action.LEFT_CLICK_AIR]:
                return

            if playerInteractEvent.getAction() in [Action.RIGHT_CLICK_BLOCK,
                    Action.LEFT_CLICK_BLOCK]:
                target = playerInteractEvent.getClickedBlock()
            else:
                target = player.buk.getTargetBlock(None, 300)

            bblock = BukkitHelper.BlockProxy.Block.fromBBlock(target)

            player.DB["targets"].append(bblock)
            player.DB["lasttargets"] = bblock
            player.buk.sendRawMessage("new Target: " + str(target))

            def _markTarget():
                import time
                bblock.saveState()
                bblock.setTypeId(20)
                time.sleep(0.1)
                bblock.restoreState()

            import thread
            thread.start_new_thread(_markTarget, ())

            if playerInteractEvent.getAction() in [Action.LEFT_CLICK_BLOCK,
                    Action.LEFT_CLICK_AIR]:
                tool = player.get("toolfunction", None)
                if tool is not None:
                    tool()

        elif material == Material.WOOD_SWORD: ##########################################
            aim = player.getAimedEntity()
            if aim is None:
                player.buk.sendRawMessage("no Target to aim")
                return

            aimlist = player.DB["aimed"]
            aimlist.append(aim)
            player.DB["lastaimed"] = aim

            import org.bukkit
            aim.playEffect(org.bukkit.EntityEffect.WOLF_HEARTS)

            if playerInteractEvent.getAction() in [Action.LEFT_CLICK_BLOCK,
                    Action.LEFT_CLICK_AIR]:
                tool = player.get("swordfunction", None)
                if tool is not None:
                    tool()

            player.buk.sendRawMessage("new Aim: %i "%id(aim) + str(aim))

    def onPyTabComplete(craftPlayer, pluginCommand, commandName, args):
        if commandName != CommandPrefix:
            return

        console = _getConsole(craftPlayer)

        if console is None:
            return None

        code = " ".join(args).replace("$", "\n")

        newText, printText = console.autocomplete(code, len(code))

        if printText is not None:
            msg(craftPlayer, str(printText))

        lastOldWord = args[-1]
        lastNewWord = newText.split(" ")[-1]

        #print "TabCompete : '%s' - '%s' - '%s'"%(code, newText, printText)

        #return "test.test"
        if len(lastOldWord) < len(lastNewWord):
            #print "Tab Completed %s -> %s"%(lastOldWord, lastNewWord)
            return lastNewWord.encode('ascii', 'ignore')

        return None


    #@CommandHandler(CommandPrefix ,usage="/<command> [pycommand]",desc="run Python Command", onTabComplete=onPyTabComplete)
    def onPyCommand(self, sender, args):

        name = sender.getName()

        console = _getConsole(sender)

        if console is None:
            msg(sender, "You are not be able to do this, do you have the 'pycommand.shell' permission?")
            return False

        def _commandthread():
            code = " ".join(args).replace("$", "\n")
            import BukkitHelper
            timeout_time = BukkitHelper.DB.get("config.pytimeout", 60 * 5)
            import BukkitHelper.Utils.timeout as timeout
            try:
                import BukkitHelper.Utils.GThread
                global ret
                ret = None
                def _exec():
                    global ret
                    ret = console.push(code)
                BukkitHelper.Utils.GThread.GThreadLimited \
                    .DirectStart(_exec, 0.3) \
                    .setName("PyCommand '%s' -> '%s'" % (name, code)) \
                    .timeout(timeout_time)
                #ret = str(timeout.callTimeout(timeout_time, console.push, code))
            except Exception, e:
                ret = str(e)

            console.reset()

            import gc
            gc.collect()

            if ret is None:
                ret = ""
            if len(ret) > 2046:
                ret = ret[:2042] + "..."

            if name == "@":
                info(ret)
            else:
                msg(sender, ret)

        #console.consoleLocals["result"] = True

        import thread#, time
        thread.start_new_thread(_commandthread, ())
        #time.sleep(0.1)

        #if "result" in console.consoleLocals:
        #    return console.consoleLocals["result"]
        return True


def _getConsole(sender):
    name = sender.getName()

    console = None

    if name == "CONSOLE":
        console = ConsoleConsole
        pass
    elif name == "@":
        #Command from CommandBlock
        if not CommandBlockConsoleDict.has_key(sender.getBlock()):
            CommandBlockConsoleDict[sender.getBlock()] = BukkitHelper.Console.customConsoleClass(BukkitHelper.DB)
        console = CommandBlockConsoleDict[sender.getBlock()]
        console.consoleLocals["cb"] = BukkitHelper.BlockProxy.Block(sender.getBlock())
    else:
        #Command from User
        bplayer = BukkitHelper.Server.getPlayer(sender.getName())
        player = BukkitHelper.Player.getPlayer(bplayer)
        if player.get("permission.pycommand.shell", False):
            console = player.getConsole()

    if console is not None:
        console.consoleLocals["listP"] = BukkitHelper.Player.getPlayerList
        console.consoleLocals.update(globals())

    return console

#######################################################
#     ____  __________  __ 
#    / __ \/ ____/ __ \/ / 
#   / /_/ / __/ / /_/ / /  
#  / _, _/ /___/ ____/ /___
# /_/ |_/_____/_/   /_____/
#
#######################################################


class TelnetAuth(TelnetRepl.Auth):
    def __init__(self, plugin):
        super(TelnetRepl.Auth, self).__init__()
        self.plugin = plugin

    def spawnConsole(self, username, client):
        bplayer = BukkitHelper.Server.getPlayer(username)
        player = BukkitHelper.Player.getPlayer(bplayer)
        console = player.getConsole()

        return ConsoleClientPlus.ConsoleClientPlus(client, console=console)

    def checkCondition(self, username, password):
        for bplayer in BukkitHelper.Server.getOnlinePlayers():
            if bplayer.getPlayerListName() == username:
                if password == "asdf":
                    return True
        return False
